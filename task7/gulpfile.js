'use strict'
const gulp = require('gulp');
const babel = require('gulp-babel');

gulp.task('babel', () =>
    gulp.src('7.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('dist'))
);
gulp.task('babel:watch', function(){
  gulp.watch('./*.js', ['babel'])
})

gulp.task('watch', ['babel:watch'])
gulp.task('default',['babel'])
